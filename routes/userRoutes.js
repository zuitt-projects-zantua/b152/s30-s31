const express = require("express");
const router = express.Router();

const userControllers = require('../controllers/userControllers')


router.post('/', userControllers.createUserController)

router.get('/', userControllers.getAllUsersController)

router.put('/updateSingleUser/:id', userControllers.updateSingleUserController)

router.get('/getSingleUser/:id', userControllers.getSingleUserController)

module.exports = router;