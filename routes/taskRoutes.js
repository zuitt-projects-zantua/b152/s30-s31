const express = require("express");

//Router() is a method from express that allow us the access to our HTTP methods routes
//Router() will act as a middleware and our routing system
const router = express.Router();

//const Task = require('../models/Task')

//expressJS routes should not handle the business logic of our application. Routes are only meant to route client request by their endpoint and method. Handling the request and responses should not be done in the routes. Instead we should have a separate functions to handle our request and response.

//import taskControllers
const taskControllers = require('../controllers/taskControllers')
//console.log(taskControllers);// obj


// /tasks/
router.post('/', taskControllers.createTaskController)

router.get('/', taskControllers.getAllTasksController)

//get a single task detail
//URL: http//localhost:4000/tasks/getSingleTask
router.get('/getSingleTask/:id', taskControllers.getSingleTaskController)


router.put('/updateTaskStatus/:id', taskControllers.updateTaskStatusController)

//router holds all our routes, it will be what we will export or import in another file.
module.exports = router;
