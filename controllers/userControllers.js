const User = require('../models/User')

module.exports.createUserController = (req,res) => {
	console.log(req.body)
	
	User.findOne({username: req.body.username})
	.then(result => {
		console.log(result)

		if(result !== null && result.username == req.body.username){
			return res.send("Duplicate User Found")
		}
		else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})
			
			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
				
		}
	})
	.catch(error => res.send(error));
}

module.exports.getAllUsersController = (req,res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

/*
module.exports.updateTaskStatusController = (req,res) => {
	let updates = {
		status: req.body.status
	}
	
	Task.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));

}
*/

module.exports.updateSingleUserController = (req,res) => {

	console.log(req.params.id)
	console.log(req.body)

	let updates = {
		username: req.body.username
	}

	User.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error))
}

module.exports.getSingleUserController = (req,res) => {
	console.log(req.params.id)

	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}