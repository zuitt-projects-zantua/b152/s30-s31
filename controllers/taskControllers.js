//import the task model in the controllers. so that our controllers or controller functions may have access to our Task model.

const Task = require('../models/Task')

//module.exports will allow us to add the controllers as methods for our module
//this controller module can be imported in other files. Modules in JS are considered objects.
module.exports.createTaskController = (req,res) => {
	console.log(req.body)

	Task.findOne({name: req.body.name})
	.then(result => {

		if(result !== null && result.name === req.body.name){
			return res.send("Duplicate Task Found")
		}
		else {
			let newTask = new Task({
				name: req.body.name,
				status: req.body.status,
			})
			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})	
	.catch(error => res.send(error));
}

module.exports.getAllTasksController = (req,res) => {

	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.getSingleTaskController = (req,res) => {

	console.log(req.params)
/*
	Task.findOne({id: req.params.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))
*/
	//model.findById() = db.collection.findOne({_id: "id"})
	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));

}

module.exports.updateTaskStatusController = (req,res) => {

	console.log(req.params.id)//check the id captured
	console.log(req.body)//check the request body

	//model.findByIdAndUpdate() = db.collection.updateOne({_id: "id"}, {$set{property: newValue}})

	//syntax
		//model.findByIdAndUpdate(id,{updates},{new:true})

	//updates object will contain the field and the value we want to update.
	let updates = {
		status: req.body.status
	}
	
	Task.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));

}