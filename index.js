const express = require("express");


/*
	Mongoose is a package which is used as an ODM, or object document mapper. It allows us to translate our JS objects into database documents for MongoDB. It allows us connection and easier manipulation of our documents.
*/
const mongoose = require("mongoose");
//All packages to be used should be required at the top of the file to avoid tampering or errors

//nmp start if the server is ready for hosting
//npm run dev if the server is under development and needs to use nodemon

const app = express();
const port = 4000;

//mongoose connection
	//mongoose.connect is the method to connect your api to your mongodb via the use of mongoose. It has 2 arguments. First, is the connection string to connect our api to our mongodb atlas. Second, is an object used to add information between mongoose and mongodb.
	//change <password> in the connection string to your db password
	
	//change myFirstDatabase to task 152
	//MongoDB upon connection and creating our first documents will create the task152 database for us

mongoose.connect("mongodb+srv://angzang:angelo@cluster0.j7r8u.mongodb.net/task152?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology:true
	});


//We will create notifications if the connection to the db is success or failed

let db = mongoose.connection;
//we add this so that when the db has an error, we will show the connection in both the terminal and in our browser for our client.

db.on('error',console.error.bind(console, "connection error."))
//Once the connection is open and successful, we will output a message in the terminal gitbash.
db.once('open',() => console.log("Connected to MongoDB"))

//Middleware - in expressjs content, are methods, functions that acts and adds features to our application
//express.json() - handle the request body of request. It handles the JSON data from our client
app.use(express.json())

const taskRoutes = require('./routes/taskRoutes');

app.use('/tasks', taskRoutes)

//import userRoutes
const userRoutes = require('./routes/userRoutes')
//group userRoutes under /users
app.use('/users', userRoutes)


app.listen(port, () => console.log(`Server is running at port ${port}`));