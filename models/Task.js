const mongoose = require("mongoose");
/*
	The naming convention for model files, are singular and capitalized names that describe the schema


*/

const taskSchema = new mongoose.Schema({
	
	name: String,
	status: String

})

module.exports = mongoose.model("Task",taskSchema);
